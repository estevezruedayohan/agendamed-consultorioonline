from Conexion_db import Conexion_db

class Crud:
    def __init__(self,host,db,user,passwd):
        self.my_conn = Conexion_db(host,db,user,passwd)
    
    def cerrar_db(self):
        self.my_conn.cerrar_db()

    def insertar_usuario(self, nombre, doc, email, passw, genero, f_creacion, estado, role):
        query = "INSERT INTO \"Usuarios\" ("+\
            "nombrecompleto, documento, emailuser, contrasena, genero, fcreacion, estado, role)"+\
            f"VALUES ('{nombre}', '{doc}', '{email}', '{passw}', '{genero}', '{f_creacion}', '{estado}', '{role}');"
        self.my_conn.escribir_db(query)

    def eliminar_usuario(self, u_id):
        query = f"DELETE FROM \"Usuarios\" WHERE idusuario = {u_id};"
        self.my_conn.escribir_db(query)

    def actualizar_usuario(self, col, valor, u_id):
        query = f"UPDATE \"Usuarios\" SET {col} = '{valor}' WHERE idusuario = {u_id};"
        self.my_conn.escribir_db(query)

    def seleccionar_usuario(self):
        query = "SELECT * FROM \"Usuarios\";"
        return self.my_conn.consultar_db(query)
    
    def seleccionar_por_id(self, u_id):
        query = f"SELECT * FROM \"Usuarios\" WHERE idusuario = {u_id};"
        return self.my_conn.consultar_db(query)
    
    def seleccionar_por_doc(self, doc):
        query = f"SELECT * FROM \"Usuarios\" WHERE documento = {doc};"
        return self.my_conn.consultar_db(query)

    # def leer_citas(self, limit):  -- AQUI ESTA LEYENDO ES LOS USUARIOS
    #     query = f"SELECT * FROM \"Usuarios\" LIMIT {limit};"
    #     return self.my_conn.consultar_db(query)
    
    def mostrar_citas_disp(self, fecha):
        query = ("Select horario_codhorario, nombrecompleto, consultorio " 
        "from \"Disponibilidad\" inner join \"Medicos\" "
        "on \"Disponibilidad\".medicos_codmedico=\"Medicos\".codmedico "
        "inner join \"Usuarios\" on \"Medicos\".idusuario=\"Usuarios\".idusuario "
        f"where \"Disponibilidad\".estado=true and \"Disponibilidad\".horario_codhorario between '{fecha} 00:00:00' and '{fecha} 23:59:59';")
        return self.my_conn.consultar_db(query)

    def agregar_cita(self, paciente, id_disponibilidad, estado, precio):
        query = f"INSERT INTO \"Citas\" (pacientes_idpacientes, disponibilidad_idDisponibilidad, estado_cita_idestado, precio) VALUES ({paciente}, {id_disponibilidad}, {estado}, {precio}); "
        query2 = f"UPDATE \"Disponibilidad\" SET estado=False WHERE iddisponibilidad = {id_disponibilidad};"
        self.my_conn.escribir_db(query)
        self.my_conn.escribir_db(query2)


    def listar_miscitas(self, idpaciente):
        query = ("Select especialidad, nombrecompleto, horario_codhorario, consultorio, descripcion "#estado_cita_idestado #especialidad, medico, fecha, consultorio, estado
        "from \"Estado_citas\" inner join \"Citas\" "
        "on  \"Estado_citas\".idestado= \"Citas\".estado_cita_idestado "
        "inner join \"Disponibilidad\" "
        "on \"Citas\".disponibilidad_iddisponibilidad=\"Disponibilidad\".iddisponibilidad "
        "inner join \"Medicos\" "
        "on \"Disponibilidad\".medicos_codmedico=\"Medicos\".codmedico "
        "inner join \"Usuarios\" "
        "on \"Medicos\".idusuario=\"Usuarios\".idusuario "
        f"where \"Citas\".pacientes_idpacientes={idpaciente} and \"Citas\".estado_cita_idestado=1;")
        return self.my_conn.consultar_db(query)

    def cancelar_cita(self, idcita):
        query = f"UPDATE \"Citas\" SET estado_cita_idestado=2 WHERE idcitas={idcita}; "
        query2 = ("UPDATE \"Disponibilidad\" SET estado = True WHERE iddisponibilidad=("
        "SELECT disponibilidad_iddisponibilidad from \"Citas\" inner join \"Disponibilidad\" "
        "on \"Citas\".disponibilidad_iddisponibilidad=\"Disponibilidad\".iddisponibilidad "
        f"where \"Citas\".idcitas={idcita});")
        self.my_conn.escribir_db(query)
        self.my_conn.escribir_db(query2)

    def consultar_idpaciente(self, documento):
        query = ("select idpaciente from \"Pacientes\" inner join \"Usuarios\" "
        "on \"Pacientes\".idusuario=\"Usuarios\".idusuario "
        f"where \"Usuarios\".documento={documento};")
        return self.my_conn.consultar_db(query)

    def consultar_iddisponibilidad(self,codhorario, consultorio):
        query = (f"select iddisponibilidad from \"Disponibilidad\" where horario_codhorario='{codhorario}' and consultorio={consultorio};")
        return self.my_conn.consultar_db(query)

    def insertar_horario(self, codhorario, fecha, hora):
        query = "INSERT INTO \"Horario\" ("+\
            "codhorario, fprogramac, horaprogramac, disponibilidad)"+\
            f"VALUES ('{codhorario}', '{fecha}', '{hora}', 'true');"
        self.my_conn.escribir_db(query)

    def eliminar_horario(self, codhorario):
        query = f"DELETE FROM \"Horario\" WHERE codhorario = '{codhorario}';"
        self.my_conn.escribir_db(query)

    def actualizar_horario(self, codhorario):
        query = f"UPDATE \"Horario\" SET disponibilidad = false WHERE codhorario = '{codhorario}';"
        self.my_conn.escribir_db(query)

    def consultar_horario(self):
        query = "SELECT * FROM \"Horario\" WHERE disponibilidad = true ORDER BY codhorario ASC;"
        return self.my_conn.consultar_db(query)
    
    def crear_disponib(self, codmedico, codhorario, consultorio):
        query = "INSERT INTO \"Disponibilidad\" ("+\
            "medicos_codmedico, horario_codhorario, consultorio, estado)"+\
            f"VALUES ({codmedico}, '{codhorario}', {consultorio}, 'true');"
        query2 = f"UPDATE \"Horario\" SET disponibilidad=false WHERE \"Horario\".codhorario='{codhorario}';"
        self.my_conn.escribir_db(query)
        self.my_conn.escribir_db(query2)

    def eliminar_disponib(self, iddisponibilidad, codhorario):
        query = f"DELETE FROM \"Disponibilidad\" WHERE iddisponibilidad = {iddisponibilidad};"
        query2 = f"UPDATE \"Horario\" SET disponibilidad = true WHERE codhorario='{codhorario}';"
        self.my_conn.escribir_db(query)
        self.my_conn.escribir_db(query2)

    def actualizar_disponib(self, iddisponib, codmedico, codhorario, consultorio, estado):
        query = f"UPDATE \"Disponibilidad\" SET medicos_codmedico={codmedico}, horario_codhorario='{codhorario}', "+\
            f"consultorio={consultorio}, estado={estado} WHERE iddisponibilidad = {iddisponib};"
        self.my_conn.escribir_db(query)

# en crud insertar formula - desde el usuario medico
    def consultar_formula(self, documento):
        query = ("SELECT idformula AS \"Formula\", horario_codhorario AS \"Fecha\",  idmedicamento AS \"Codigo\", "
            "nombre AS \"Medicamento\", nombrecomercial AS \"COMERCIAL\", presentacion, cantidad, posologia "
            "FROM \"Formulacion\" INNER JOIN \"Medicamentos\" ON \"Formulacion\".medicamentos_idmedicamento=\"Medicamentos\".idmedicamento "
            "INNER JOIN \"Citas\" ON \"Formulacion\".citas_idcitas=\"Citas\".idcitas "
            "INNER JOIN \"Disponibilidad\" ON \"Citas\".disponibilidad_iddisponibilidad=\"Disponibilidad\".iddisponibilidad "
            "INNER JOIN \"Pacientes\" ON \"Citas\".pacientes_idpacientes=\"Pacientes\".idpaciente "
            "INNER JOIN \"Usuarios\" ON \"Pacientes\".idusuario=\"Usuarios\".idusuario "
            f"WHERE documento={documento} ORDER BY \"Formula\";")
        return self.my_conn.consultar_db(query)
    
    def insertar_formula(self, idcitas, idmedicamento, cantidad, posologia): # int, int, int, varchar(50)
        query = "INSERT INTO \"Formulacion\" ("+\
            "citas_idcitas, medicamentos_idmedicamento, cantidad, posologia)"+\
            f"VALUES ({idcitas}, {idmedicamento}, {cantidad}, '{posologia}');"
        self.my_conn.escribir_db(query)

    def eliminar_formula(self, idformula):
        query = f"DELETE FROM \"Formulacion\" WHERE idformula = {idformula};"
        self.my_conn.escribir_db(query)

    def actualizar_formula(self, idformula, idmedicamento, cantidad, posologia):#medicamentos_idmedicamento, cantidad, posologia
        query = f"UPDATE \"Formulacion\" SET medicamentos_idmedicamento={idmedicamento}, cantidad={cantidad}, "+\
            f"posologia='{posologia}' WHERE idformula = {idformula};"
        self.my_conn.escribir_db(query)

    # en crud insertar seguido - desde el usuario administrador
    def consultar_medicamento(self, parte): 
        query = ("SELECT * FROM \"Medicamentos\" WHERE LOWER(nombre) LIKE LOWER('%"
        f"{parte}"
        "%') ORDER BY idmedicamento;")
        return self.my_conn.consultar_db(query)

    def insertar_medicamento(self, nombre, presentacion, nombrecomercial): # varchar(120) todos creo
        query = "INSERT INTO \"Medicamentos\" ("+\
            "nombre, presentacion, nombrecomercial)"+\
            f"VALUES ('{nombre}', '{presentacion}', '{nombrecomercial}');"
        self.my_conn.escribir_db(query)

    def eliminar_medicamento(self, idmedicamento): # OBSERVACION: Si hay formulas ya creadas con este medicamento, el medicamento no se podrá borrar
        query = f"DELETE FROM \"Medicamentos\" WHERE idmedicamento = {idmedicamento};"
        self.my_conn.escribir_db(query)

    def actualizar_medicamento(self, idmedicamento, nombre, presentacion, nombrecomercial):#nombre, presentacion, nombrecomercial
        query = f"UPDATE \"Medicamentos\" SET nombre='{nombre}', presentacion='{presentacion}', nombrecomercial='{nombrecomercial}' "+\
            f"WHERE idmedicamento = {idmedicamento};"
        self.my_conn.escribir_db(query)