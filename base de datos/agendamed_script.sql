CREATE TABLE Citas (
  idCitas INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Pacientes_documento INTEGER UNSIGNED NOT NULL,
  Disponibilidad_idDisponibilidad INTEGER UNSIGNED NOT NULL,
  Estado_cita_idEstado INTEGER UNSIGNED NOT NULL,
  precio DOUBLE NULL,
  PRIMARY KEY(idCitas),
  INDEX Citas_FKIndex1(Estado_cita_idEstado),
  INDEX Citas_FKIndex2(Disponibilidad_idDisponibilidad),
  INDEX Citas_FKIndex3(Pacientes_documento)
)
TYPE=InnoDB;

CREATE TABLE Disponibilidad (
  idDisponibilidad INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Medicos_codMedico INTEGER UNSIGNED NOT NULL,
  Horario_codHorario DATETIME NOT NULL,
  codCentro INTEGER UNSIGNED NULL,
  PRIMARY KEY(idDisponibilidad),
  INDEX Disponibilidad_citas_FKIndex2(Horario_codHorario),
  INDEX Disponibilidad_citas_FKIndex2(Medicos_codMedico)
)
TYPE=InnoDB;

CREATE TABLE Estado_cita (
  idEstado INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  descripcion ENUM NOT NULL DEFAULT Asignada,Aplazada,Cancelada,
  PRIMARY KEY(idEstado)
)
TYPE=InnoDB;

CREATE TABLE Formulacion (
  idFormula INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Citas_idCitas INTEGER UNSIGNED NOT NULL,
  Medicamentos_idMedicamento INTEGER UNSIGNED NOT NULL,
  cantidad INTEGER UNSIGNED NOT NULL,
  posologia VARCHAR(50) NOT NULL,
  PRIMARY KEY(idFormula),
  INDEX Formulacion_FKIndex1(Medicamentos_idMedicamento),
  INDEX Formulacion_FKIndex2(Citas_idCitas)
)
TYPE=InnoDB;

CREATE TABLE Horario (
  codHorario DATETIME NOT NULL,
  fechaProgramacion DATETIME NOT NULL,
  disponibilidad ENUM NOT NULL DEFAULT SI,NO,
  PRIMARY KEY(codHorario)
)
TYPE=InnoDB;

CREATE TABLE Medicamentos (
  idMedicamento INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(40) NOT NULL,
  presentacion VARCHAR(40) NOT NULL,
  nombreComercial VARCHAR(40) NULL,
  PRIMARY KEY(idMedicamento)
)
TYPE=InnoDB;

CREATE TABLE Medicos (
  codMedico INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombres VARCHAR(40) NOT NULL,
  apellidos VARCHAR(40) NOT NULL,
  fNacimiento DATE NOT NULL,
  documento INTEGER UNSIGNED NOT NULL,
  sexo ENUM NOT NULL DEFAULT F,M,
  Usuarios_idUsuario INTEGER UNSIGNED NOT NULL,
  codEspecialidad INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(codMedico),
  INDEX Medicos_FKIndex1(Usuarios_idUsuario)
)
TYPE=InnoDB;

CREATE TABLE Pacientes (
  documento INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nombres VARCHAR(40) NOT NULL,
  apellidos VARCHAR(40) NOT NULL,
  fNacimiento DATE NOT NULL,
  sexo CHAR NOT NULL,
  Usuarios_idUsuario INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(documento),
  INDEX Pacientes_FKIndex1(Usuarios_idUsuario)
)
TYPE=InnoDB;

CREATE TABLE Usuarios (
  idUsuario INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  fCreacion DATE NOT NULL,
  estado ENUM NOT NULL DEFAULT Activo,Inactivo,
  password_2 VARCHAR(10) NOT NULL,
  PRIMARY KEY(idUsuario)
)
TYPE=InnoDB;


