Este documento tiene como fin aclarar sobre los archivos de la base de datos de esta carpeta.

El diseño o modelo fue realizado en DBDesigner 4 de fabForce.net, debido a aún no conocemos 
todas las funcionalidades del actual software utilizado en el desarrollo de este ciclo - pgAdmin4 -.

Se generó un archivo .html como reporte de las características de las entidades y atributos, se 
generó un script para las tablas de la base de datos, y el archivo xml como modelo del software
DBDesigner para que sirva de soporte en caso que se requiera modificar el modelo de la base de
datos del aplicativo AgendaMed.

