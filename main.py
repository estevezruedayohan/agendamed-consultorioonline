from flask import Flask,request,make_response,redirect
from flask.wrappers import Response #clase Flask de extension flask
app = Flask(__name__) #instancia de Flask
from Crud import Crud
import os
import json

@app.route('/')  # ruta index, principal.
def inicio():    # funcion de inicio
    response = '<b>:: ¡AgendaMed - Consultorio Online! ::</b><hr><br>\
        Rutas disponibles:<br><br>* /leer_citas <br> * /crear_usuario\
            <br> * /consultar_usuario'
    return response

# AQUI ESTA LEYENDO LOS USUARIOS DE LA TABLA USUARIOS
# @app.route('/leer_citas')    # ruta para leer citas (Colocar la cantidad el el parámetro)
# def leer():                  # funcion para leer citas ( tabla usuarios )
#     user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))

#     agenda_citas = user_crud.leer_citas(10) # Parámetro con la cantidad de citas deseadas
#     citas = [] 
#     for registro in agenda_citas:
#         citas.append({"Documento":registro[2], "Nombre":registro[1] ,"Fecha_creacion":registro[6],
#          "Estado":registro[7], "Role":registro[8]+'<br><hr>'})
#     resp = json.dumps(citas, default=str)

#     user_crud.cerrar_db()
#     return f'GESTIÓN DE USUARIOS: <br><hr>{resp}'

@app.route('/citas_disponibles')    # ruta para leer citas (Colocar la cantidad el el parámetro)
def leer():                  # funcion para leer citas ( tabla usuarios )
    
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))

    fecha = '2021-09-28'#input("Ingrese la fecha a consultar: ")
    
    agenda_citas = user_crud.mostrar_citas_disp(fecha) # Parámetro con la cantidad de citas deseadas
    citas = [] 
    for registro in agenda_citas:
        citas.append({"Cita":registro[0], "Medico":registro[1] ,"Consultorio":registro[2],"Accion":'<a href=>Asignar</a><br>'}) 

    resp = json.dumps(citas, default=str)

    user_crud.cerrar_db()
    return f'GESTIÓN DE CITAS: <br><hr>{resp}'

@app.route('/asignar_cita')    # ruta para leer citas (Colocar la cantidad el el parámetro)
def asignarcita():                  # funcion para leer citas ( tabla usuarios )
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    
    cedpaciente=187770085
    paciente=user_crud.consultar_idpaciente(cedpaciente) # está opcionado para que se busque en la bd por método consultar_idpaciente
    disponibilidad=user_crud.consultar_iddisponibilidad('2021-09-28 06:00:00',1) #2021-09-28 06:00:00 
    paciente=paciente[0][0]
    disponibilidad = disponibilidad[0][0]
    estado=1
    precio= 14000
    
    user_crud.agregar_cita(paciente, disponibilidad, estado, precio) # agregamos los datos de id_paciente, id_disponibilidad,estado(1-Asignada),precio
    
    user_crud.cerrar_db() # cerramos la conexion
    resp = '... confirmar operación en la base de datos!'
    return f'GESTIÓN DE CITAS: <br><hr>{resp}'

@app.route('/mis_citas')    # ruta para leer citas del usuario
def mis_citas():                  # funcion para leer citas 
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    idpaciente=int(input("Ingrese el id del paciente: ")) #id del usuario cuyas citas se van a consultar
    miscitas = user_crud.listar_miscitas(idpaciente) 
    citas = [] 
    for registro in miscitas:
        citas.append({"Especialidad":registro[0], "Medico":registro[1] ,"Horario":registro[2],"Consultorio":registro[3],"Estado":registro[4]," ":'<br>'}) 

    resp = json.dumps(citas, default=str) #, default=str

    user_crud.cerrar_db() # cerramos la conexion
    return f'GESTIÓN DE CITAS - CONSULTAR Y CANCELAR: <br><hr>{resp}'

@app.route('/cancelar_cita')    # ruta para leer citas (Colocar la cantidad el el parámetro)
def cancelarcita():                  # funcion para leer citas ( tabla usuarios )
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))

    idcita=int(input("Indique el id de la cita a cancelar: "))
    user_crud.cancelar_cita(idcita) # agregamos los datos de id_paciente, id_disponibilidad,estado(1-Asignada),precio
    
    user_crud.cerrar_db() # cerramos la conexion
    resp = '... confirmar operación en la base de datos!'
    return f'GESTIÓN DE CITAS - Cancelar Cita: <br><hr>{resp}'


@app.route('/crear_usuario')  # ruta para crear, eliminar y actualizar un usuario
def crear():                  # funcion para gestión de usuarios
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))

    # (1) INSERT
    # user_crud.insertar_usuario('Alex Zurdo', 1054546321, 'alexzurdo@gmail.com', 'bb8sdj1d47yo', 'M', '1995-02-28', False, 'Paciente')
    
    # (2) DELETE
    # user_crud.eliminar_usuario(163)

    # (3) UPDATE
    # user_crud.actualizar_usuario('nombrecompleto', 'Esneider Ortiz', 170)
 
    user_crud.cerrar_db()
    resp = '... confirmar operación en la base de datos!'
    return f'GESTIÓN DE USUARIOS: <br><hr>{resp}'


@app.route('/consultar_usuario')  # ruta para seleccionar un usuario por id, por documento, o consultar todos los usuarios
def consultar():                  # funcion para consultar usuarios
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))

    # (1) SELECT WHERE id (?)
    # user = user_crud.seleccionar_por_id(160)
    # resp = json.dumps(user, default=str)

    # (2) SELECT WHERE documento (?)
    # user = user_crud.seleccionar_por_doc(48937945)
    # resp = json.dumps(user, default=str)

    # (3) SELECT * FROM
    users = user_crud.seleccionar_usuario()
    datos = []
    for registro in users:
        datos.append({"id":registro[0], "Nombre":registro[1],
        "Documento":registro[2],"Correo":registro[3],"Contrasena":registro[4], 
        "Genero":registro[5], "Fecha_creacion":registro[6], "Estado":registro[7], "Rol":registro[8]+'<br>'})
    resp = json.dumps(datos, default=str)

    user_crud.cerrar_db()
    return f'GESTIÓN DE USUARIOS: <br><hr>{resp}'


@app.route('/crear_horario')  # ruta para crear, eliminar y actualizar un usuario
def crearhorario():                  # funcion para gestión de usuarios
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    
    opcion = 4
    fecha='2021-10-01'
    hora='06:30:00'
    codhorario=fecha+' '+hora
    resp = '... confirmar operación en la base de datos!'

    if (opcion == 1):
        user_crud.insertar_horario(codhorario, fecha, hora)
    if (opcion == 2):
        user_crud.eliminar_horario(codhorario)
    if (opcion == 3):
        user_crud.actualizar_horario(codhorario)
    if (opcion == 4):
        horarios = user_crud.consultar_horario()
        disponibilidad = [] 
        for registro in horarios:
            disponibilidad.append({"Codhorario":registro[0],"Accion":'<a href=>Asignar</a><br>'}) 
        resp = json.dumps(disponibilidad, default=str)
 
    user_crud.cerrar_db()
    return f'GESTIÓN DE HORARIOS: <br><hr>{resp}'

@app.route('/crear_disponibilidad')  # ruta para crear, E y actualizar una disponibilidad de cita
def creardisponib():                  
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    
    opcion = 4
    codmedico = 6
    codhorario='2021-10-01 06:30:00'
    consultorio = 1
    estado = False
    resp = '... confirmar operación en la base de datos!'

    if (opcion == 1):
        user_crud.crear_disponib(codmedico, codhorario, consultorio)
    if (opcion == 2):
        iddisponib = user_crud.consultar_iddisponibilidad(codhorario, consultorio)
        iddisponib = iddisponib[0][0]
        user_crud.eliminar_disponib(iddisponib, codhorario)
    if (opcion == 3):
        iddisponib = user_crud.consultar_iddisponibilidad(codhorario, consultorio)
        iddisponib = iddisponib[0][0]
        user_crud.actualizar_disponib(iddisponib, codmedico, codhorario, consultorio, estado)
    if (opcion == 4):
        fecha = '2021-09-28' #aqui se puede redigir el codigo hacia el procedimiento /citas_disponibles
        disponibilidades = user_crud.mostrar_citas_disp(fecha)
        disponibilidad = [] 
        for registro in disponibilidades:
            disponibilidad.append({"Codhorario":registro[0],"Medico":registro[1], "Consultorio":registro[2],"Accion":'<a href=>Asignar</a><br>'}) 
        resp = json.dumps(disponibilidad, default=str)
 
    user_crud.cerrar_db()
    return f'GESTIÓN DE DISPONIBILIDADES: <br><hr>{resp}'

@app.route('/gestion_formulas')  # ruta para crud de una formula
def gestionarformulas():                  
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    
    opcion = 4
    idcita= 34
    idmedicamento = 82
    cantidad = 10
    idformula = 51
    documento = 187770085   #usuario 167 cc 982132977
    posologia = "1 tableta vo cada 8 horas x 10 dias"
    resp = '... confirmar operación en la base de datos!'

    if (opcion == 1):
        user_crud.insertar_formula(idcita, idmedicamento,cantidad,posologia) #
    if (opcion == 2):
        user_crud.eliminar_formula(idformula)
    if (opcion == 3):
        user_crud.actualizar_formula(idformula, idmedicamento, cantidad, posologia)
    if (opcion == 4):
        formulas = user_crud.consultar_formula(documento)
        listaformulas = [] 
        for registro in formulas:
            listaformulas.append({"Formula":registro[0],"Fecha":registro[1], "Codigo":registro[2], "Medicamento":registro[3],
            "COMERCIAL":registro[4],"Presentacion":registro[5],"Cantidad":registro[6],"Posologia":registro[7],
            "Accion":'<a href=>Modificar</a> - <a href=>Eliminar</a><br>'}) 
        resp = json.dumps(listaformulas, default=str)
 
    user_crud.cerrar_db()
    return f'GESTIÓN DE FORMULAS: <br><hr>{resp}'

@app.route('/gestion_medicamentos')  # ruta para crud de una formula
def gestionarmedicamentos():                  
    user_crud = Crud(os.getenv('HOST'), os.getenv('DB'), os.getenv('USER'), os.getenv('PASSW'))
    
    opcion = 4
    idmedicamento = 134
    nombre = 'Sertralina'
    presentacion = 'Tableta recubierta 50 mg'
    nombrecomercial = 'Sertranquil lite'
    parte='aceta'
    resp = '... confirmar operación en la base de datos!'

    if (opcion == 1):
        user_crud.insertar_medicamento(nombre, presentacion, nombrecomercial) #
    if (opcion == 2):
        user_crud.eliminar_medicamento(idmedicamento)
    if (opcion == 3):
        user_crud.actualizar_medicamento(idmedicamento, nombre, presentacion, nombrecomercial)
    if (opcion == 4):
        medicamentos = user_crud.consultar_medicamento(parte)
        listamedicamentos = [] 
        for registro in medicamentos:
            listamedicamentos.append({"CODIGO":registro[0],"MEDICAMENTO":registro[1], "PRESENTACION":registro[2], 
            "NOMBRE_COMERCIAL":registro[3],"Accion":'<a href=>Modificar</a> - <a href=>Eliminar</a><br>'}) 
        resp = json.dumps(listamedicamentos, default=str)
 
    user_crud.cerrar_db()
    return f'GESTIÓN DE MEDICAMENTOS: <br><hr>{resp}'

if __name__=="__main__":                   # punto de partida, donde python se empieza a ejecutar
	while(True):
		print("Starting web server")
		app.run(debug=True,host='0.0.0.0') #arranca el servidor, 0.0.0.0->localhost
        #en el navegador se accede localhost:5000/
